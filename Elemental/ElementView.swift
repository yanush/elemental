//
//  ElementView.swift
//  Elemental
//
//  Created by Eric Yanush on 2016-02-03.
//  Copyright © 2016 EricYanush. All rights reserved.
//

import Foundation
import UIKit

protocol ElementViewSelectDelegate {
    func userDidSelectElementView(view: ElementView)
}

class ElementView: UIView {
    
    var stack: UIStackView
    
    var symbolLabel: UILabel
    var numberLabel: UILabel
    var massLabel: UILabel
    
    private var el: Element
    var element: Element {
        get {
            return el
        }
        set {
            el = newValue
            if (el.group != .Empty) {
                symbolLabel.text = el.symbol
                numberLabel.text = String(el.number)
                massLabel.text = String(el.mass)
                //Enable border for actual el
                self.layer.borderColor = UIColor.blackColor().CGColor
                self.layer.borderWidth = 1.0
            }
            else {
                symbolLabel.text = ""
                numberLabel.text = ""
                massLabel.text = ""
                //Disable the border for empty el
                self.layer.borderWidth = 0.0
            }
        }
    }
    
    private var gestureRecognizer: UITapGestureRecognizer
    var touchDelegate: ElementViewSelectDelegate?
    
    override init(frame: CGRect) {
        self.stack = UIStackView();
        self.symbolLabel = UILabel();
        self.numberLabel = UILabel();
        self.massLabel = UILabel();
        self.el = Element.empty()
        self.gestureRecognizer = UITapGestureRecognizer()
        
        super.init(frame: frame);
        self.heightAnchor.constraintEqualToConstant(frame.height).active = true
        self.widthAnchor.constraintEqualToConstant(frame.width).active = true
        self.layer.cornerRadius = 3.0
        self.layer.masksToBounds = true
        setupStack()
        setupLabels()
        setupTouchDetect()
    }

    convenience init(frame: CGRect, element: Element) {
        self.init(frame: frame)
        self.element = element
    }
    
    convenience init(element: Element) {
        self.init()
        setupLabels()
        self.element = element
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupStack() {
        let f = CGRectMake(0, 0, self.frame.width, self.frame.height)
        stack.frame = f
        stack.alignment = .Fill
        stack.axis = .Vertical
        stack.distribution = .FillEqually
        self.addSubview(self.stack)
    }
    
    func setupLabels() {
        numberLabel.font = UIFont.systemFontOfSize(10.0);
        numberLabel.numberOfLines = 1;
        numberLabel.adjustsFontSizeToFitWidth = true
        numberLabel.textAlignment = .Center
        
        symbolLabel.font = UIFont.boldSystemFontOfSize(20.0)
        symbolLabel.numberOfLines = 1;
        symbolLabel.adjustsFontSizeToFitWidth = true
        symbolLabel.textAlignment = .Center
        
        massLabel.font = UIFont.systemFontOfSize(10.0);
        massLabel.numberOfLines = 1;
        massLabel.adjustsFontSizeToFitWidth = true
        massLabel.textAlignment = .Center
        
        stack.addArrangedSubview(numberLabel);
        stack.addArrangedSubview(symbolLabel);
        stack.addArrangedSubview(massLabel);
    }
    
    func handleTouch(sender: UITapGestureRecognizer) {
        if let delegate = touchDelegate {
            delegate.userDidSelectElementView(self)
        }
    }
    
    func setupTouchDetect() {
        gestureRecognizer.addTarget(self, action: "handleTouch:")
        self.addGestureRecognizer(gestureRecognizer)
    }
}
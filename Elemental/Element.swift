//
//  Element.swift
//  Elemental
//
//  Created by Eric Yanush on 2016-02-03.
//  Copyright © 2016 EricYanush. All rights reserved.
//

import Foundation

struct Element {
    
    enum Group: String {
        case AlkaliMetals         = "Alkali metals"
        case AlkalineEarthMetals  = "Alkaline earth metals"
        case TransitionMetals     = "Transition metals"
        case Icosa­gens            = "Icosa­gens"
        case Crys­tallo­gens        = "Crys­tallo­gens"
        case Pnicto­gens           = "Pnicto­gens"
        case Chal­co­gens           = "Chal­co­gens"
        case Halo­gens             = "Halo­gens"
        case NobleGases           = "Noble gases"
        case Actinoid             = "Actinoid"
        case Lanthanoid           = "Lanthanoid"
        case Empty                = ""
    }
    
    enum ElementGroup: String {
        case Lith­ium    = "Lith­ium"
        case Beryl­lium  = "Beryl­lium"
        case Scan­dium   = "Scan­dium"
        case Titan­ium   = "Titan­ium"
        case Vana­dium   = "Vana­dium"
        case Chro­mium   = "Chro­mium"
        case Man­ga­nese  = "Man­ga­nese"
        case Iron       = "Iron"
        case Co­balt     = "Co­balt"
        case Nickel     = "Nickel"
        case Cop­per     = "Cop­per"
        case Zinc       = "Zinc"
        case Boron      = "Boron"
        case Car­bon     = "Car­bon"
        case Nitro­gen   = "Nitro­gen"
        case Oxy­gen     = "Oxy­gen"
        case Fluor­ine   = "Fluor­ine"
        case Helium     = "Helium"
        case Actinoid   = "Actinoid"
        case Lanthanoid = "Lanthanoid"
        case Empty      = ""
    }
    
    static let ElementGroupSequence: [ElementGroup] = [.Lith­ium, .Beryl­lium, .Scan­dium, .Titan­ium,
                                                       .Vana­dium, .Chro­mium, .Man­ga­nese, .Iron,
                                                       .Co­balt, .Nickel, .Cop­per, .Zinc, .Boron,
                                                       .Car­bon, .Nitro­gen, .Oxy­gen, .Fluor­ine,
                                                       .Helium]
    
    enum OrbitalType {
        case S
        case P
        case D
        case F
        case Empty
    }
    
    var name: String
    var symbol: String
    var mass: Float
    var number: Int
    var group: Group
    var groupEl: ElementGroup
    var orbital: OrbitalType
    
    static func empty() -> Element {
        return Element(name: "", symbol: "", mass: 0, number: 0, group: .Empty, groupEl: .Empty, orbital: .Empty)
    }
}
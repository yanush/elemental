//
//  PeriodTableViewController.swift
//  Elemental
//
//  Created by Eric Yanush on 2016-02-02.
//  Copyright © 2016 EricYanush. All rights reserved.
//

import Foundation
import UIKit

class PeriodicTableViewController: UIViewController, ElementViewSelectDelegate {
    
    var elementViewSq: CGFloat = 56.0
    
    var mainTableStack: UIStackView = UIStackView()
    var extendedTableStack: UIStackView = UIStackView()
    var tableStack: UIStackView = UIStackView()
    var groupStacks: [UIStackView] = []
    var extendedPeriodStacks: [UIStackView] = []
    
    lazy var elementPopover: ElementPopoverController = ElementPopoverController()
    
    override func viewDidLoad() {
        elementViewSq = self.view.frame.width / 18
        setupTable()
        self.view.addSubview(tableStack)
    }
    
    func setupTable() {
        let tableHeight = (elementViewSq * 7) + 20 + (elementViewSq * 2)
        let yPos = (self.view.frame.height - tableHeight) / 2
        tableStack.frame = CGRectMake(0, yPos, self.view.frame.width, tableHeight)
        tableStack.alignment = .Leading
        tableStack.distribution = .EqualSpacing
        tableStack.axis = .Vertical
        tableStack.spacing = 20
        
        for seq in Element.ElementGroupSequence {
            var els = Elements.filter({ (e) -> Bool in
                return e.groupEl == seq
            })
            
            switch (seq) {
            case .Scan­dium:
                //Insert two placeholders at the end of the scandium family
                els.insertContentsOf([Element.empty(), Element.empty()], at: els.count)
                break
            default:
                break
            }
            groupStacks.append(createStackViewFromElements(els))
        }
        mainTableStack = UIStackView(arrangedSubviews: groupStacks)
        mainTableStack.frame = CGRectMake(0, 0, self.view.frame.width, elementViewSq * 7)
        mainTableStack.alignment = .Fill
        mainTableStack.distribution = .FillEqually
        mainTableStack.axis = .Horizontal
        tableStack.addArrangedSubview(mainTableStack)
        
        setupExtendedTable()
    }
    
    func setupExtendedTable() {
        
        let lantEls = Elements.filter { (e) -> Bool in
            return e.groupEl == .Lanthanoid
        }
        extendedPeriodStacks.append(createStackViewFromElements(lantEls, axis: .Horizontal))
        
        let actEls = Elements.filter { (e) -> Bool in
            return e.groupEl == .Actinoid
        }
        extendedPeriodStacks.append(createStackViewFromElements(actEls, axis: .Horizontal))
        
        extendedTableStack = UIStackView(arrangedSubviews: extendedPeriodStacks)
        extendedTableStack.frame = CGRectMake(0, 0, elementViewSq * 15, elementViewSq * 2)
        extendedTableStack.alignment = .Fill
        extendedTableStack.distribution = .FillEqually
        extendedTableStack.axis = .Vertical
        tableStack.addArrangedSubview(extendedTableStack)
    }
    
    func createStackViewFromElements(var els: [Element], axis: UILayoutConstraintAxis = .Vertical) -> UIStackView {
        //Add padding at beginning when necessary
        if (els.count < 7) {
            let padd = [Element](count: 7 - els.count, repeatedValue: Element.empty())
            els.insertContentsOf(padd, at: 0)
        }
        //Pad the extended groups by 2
        else if (els.count == 15) {
            let pad = [Element](count: 2, repeatedValue: Element.empty())
            els.insertContentsOf(pad, at: 0)
        }
        
        let elViews = els.enumerate().map { (item: (index: Int, element: Element)) -> ElementView in
            let f = CGRectMake(0, 0, elementViewSq, elementViewSq)
            let e = ElementView(frame: f, element: item.element)
            e.touchDelegate = self
            return e
        }
        
        let f = CGRectMake(0, 0, elementViewSq, 7 * elementViewSq)
        let stack = UIStackView(arrangedSubviews: elViews)
        stack.frame = f
        stack.alignment = .Center
        stack.axis = axis
        stack.distribution = .EqualCentering
        
        return stack
    }
    
    func userDidSelectElementView(view: ElementView) {
        elementPopover.modalPresentationStyle = .FormSheet
        elementPopover.popoverPresentationController?.sourceView = view
        elementPopover.element = view.element
        self.presentViewController(elementPopover, animated: true, completion: nil)
    }
}
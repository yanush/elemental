//
//  ElementPopoverController.swift
//  Elemental
//
//  Created by Eric Yanush on 2016-02-04.
//  Copyright © 2016 EricYanush. All rights reserved.
//

import UIKit
import Foundation
import WebKit

class ElementPopoverController: UIViewController, WKNavigationDelegate {
    
    private var navBar: UINavigationBar = UINavigationBar()
    private var navItem: UINavigationItem = UINavigationItem()
    private var loadingSpinner: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    private var webView: WKWebView = WKWebView()
    
    private var targetElement: Element = Element.empty()
    var element: Element {
        get {
            return targetElement
        }
        
        set {
            targetElement = newValue
            navItem.title = targetElement.name
            let request = NSURLRequest(URL: NSURL(string: "https://wikipedia.org/wiki/\(targetElement.name)")!)
            loadingSpinner.startAnimating()
            webView.loadRequest(request)
        }
    }
    
    override func viewDidLoad() {
        navItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: "donePressed:")
        navItem.rightBarButtonItem = UIBarButtonItem(customView: loadingSpinner)
        loadingSpinner.hidesWhenStopped = true
        navBar.items?.append(navItem)
        navBar.barStyle = .Default
        view.addSubview(navBar)
        navBar.translatesAutoresizingMaskIntoConstraints = false
        navBar.topAnchor.constraintEqualToAnchor(view.topAnchor).active = true
        navBar.widthAnchor.constraintEqualToAnchor(view.widthAnchor).active = true
        navBar.heightAnchor.constraintEqualToConstant(44.0).active = true
        
        view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraintEqualToAnchor(navBar.bottomAnchor).active = true
        webView.bottomAnchor.constraintEqualToAnchor(view.bottomAnchor).active = true
        webView.widthAnchor.constraintEqualToAnchor(view.widthAnchor).active = true
        webView.allowsBackForwardNavigationGestures = false
        webView.navigationDelegate = self
    }
    
    func donePressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
        let req = NSURLRequest(URL: NSURL(string: "about:blank")!)
        webView.loadRequest(req)
    }
    
    func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        loadingSpinner.stopAnimating()
    }
    
}
